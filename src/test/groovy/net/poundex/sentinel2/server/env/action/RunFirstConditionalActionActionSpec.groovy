package net.poundex.sentinel2.server.env.action

import net.poundex.sentinel2.server.SentinelEnvironment
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate
import spock.lang.Specification
import spock.lang.Subject

class RunFirstConditionalActionActionSpec extends Specification {

	EnvironmentPredicate truePred = Stub() {
		test(_) >> true
	}
	EnvironmentPredicate falsePred = Stub() {
		test(_) >> false
	}
	
	ConditionalAction action1 = Mock() {
		getPredicate() >> falsePred
	}
	ConditionalAction action2 = Mock() {
		getPredicate() >> truePred
	}
	ConditionalAction action3 = Mock() {
		getPredicate() >> falsePred
	}
	ConditionalAction action4 = Mock() {
		getPredicate() >> truePred
	}
	
	SentinelEnvironment sentinelEnvironment = Stub()

	@Subject
	RunFirstConditionalActionAction action =
			new RunFirstConditionalActionAction("id", "name", 
					[action1, action2, action3, action4])

	void "Runs first child action with passing predicate"() {
		when:
		action.run(sentinelEnvironment)

		then:
		0 * action1.run(_)
		1 * action2.run(sentinelEnvironment)
		0 * action3.run(_)
		0 * action4.run(_)
	}
}
