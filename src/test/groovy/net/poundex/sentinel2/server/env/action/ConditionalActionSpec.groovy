package net.poundex.sentinel2.server.env.action

import net.poundex.sentinel2.server.SentinelEnvironment
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate
import spock.lang.Specification
import spock.lang.Subject

class ConditionalActionSpec extends Specification {
	
	EnvironmentPredicate environmentPredicate = Stub()
	Action delegate = Mock()
	SentinelEnvironment environment = Stub()
	
	@Subject
	ConditionalAction action = new ConditionalAction(null, null, environmentPredicate, delegate)
	
	void "Does not run action if predicate fails"() {
		given:
		environmentPredicate.test(environment) >> false
		
		when:
		action.run(environment)
		
		then:
		0 * delegate.run(_)
	}

	void "Runs action if predicate passes"() {
		given:
		environmentPredicate.test(environment) >> true

		when:
		action.run(environment)

		then:
		1 * delegate.run(environment)
	}
}
