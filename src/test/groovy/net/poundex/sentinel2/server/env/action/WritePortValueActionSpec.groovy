package net.poundex.sentinel2.server.env.action

import net.poundex.sentinel2.server.SentinelEnvironment
import net.poundex.sentinel2.server.appliance.Appliance
import net.poundex.sentinel2.server.device.Device
import net.poundex.sentinel2.server.device.DeviceManager
import net.poundex.sentinel2.server.device.port.WritableDevicePort
import net.poundex.sentinel2.server.env.value.Value
import spock.lang.Specification
import spock.lang.Subject

class WritePortValueActionSpec extends Specification {
	
	Appliance appliance = Appliance.builder().deviceId(URI.create("device://device/self")).build()
	Value<? extends Value> value = Stub()
	WritableDevicePort devicePort = Mock()
	SentinelEnvironment environment = Stub() {
		getDeviceManager() >> Stub(DeviceManager) {
			getDevice(URI.create("device://device/self")) >> Optional.of(Stub(Device) {
				getPort("portName".toURI()) >> Optional.of(devicePort)
			})
		}
	}
	
	@Subject
	WritePortValueAction action = 
			new WritePortValueAction("id", "name", appliance, "portName".toURI(), value)
	
	void "Write port values to correct device port"() {
		when:
		action.run(environment)
		
		then:
		1 * devicePort.writeValue(value)
	}
}
