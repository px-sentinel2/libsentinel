package net.poundex.sentinel2.server.env.zonevariable

import net.poundex.sentinel2.server.env.value.NumericValue
import net.poundex.sentinel2.server.env.value.ValueWithMetadata
import spock.lang.Specification

import java.time.Instant

class NumericFoldStrategySpec extends Specification {
	
	private static final NumericValue ZERO = new NumericValue(0)
	private static final NumericValue ONE = new NumericValue(1)
	private static final NumericValue TWO = new NumericValue(2)
	
	void "Average folds zone variables"(Collection<ValueWithMetadata<NumericValue>> input, NumericValue expected) {
		expect:
		NumericZoneVariable.NumericFoldStrategy.AVERAGE.fold(input) == expected

		where:
		input || expected
		withMetadata([ZERO])      || ZERO
		withMetadata([ZERO, TWO]) || ONE
		withMetadata([])          || ZERO
	}
	
	private static List<ValueWithMetadata<NumericValue>> withMetadata(List<NumericValue> values) {
		return values.collect { new ValueWithMetadata<>(it, Instant.now())}
	}
}
