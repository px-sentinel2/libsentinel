package net.poundex.sentinel2.server.env.action

import net.poundex.sentinel2.server.SentinelEnvironment
import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableManager
import spock.lang.Specification
import spock.lang.Subject

class CopyZoneVariableActionSpec extends Specification {

	private static final Zone ZONE_1 = Zone.builder().build()
	private static final Zone ZONE_2 = Zone.builder().build()
	
	ZoneVariable ZONE_VARIABLE_1 = Stub()
	ZoneVariable ZONE_VARIABLE_2 = Stub()
	Value VALUE = Stub()

	ZoneVariableManager zoneVariableManager = Mock()

	SentinelEnvironment sentinelEnvironment = Stub() {
		getZoneVariableManager() >> zoneVariableManager
	}
	
	@Subject
	CopyZoneVariableAction action = new CopyZoneVariableAction(null, null, ZONE_1, ZONE_VARIABLE_1, ZONE_2, ZONE_VARIABLE_2)
	
	void "Copies zone variable"() {
		when:
		action.run(sentinelEnvironment)

		then:
		1 * zoneVariableManager.getValue(ZONE_VARIABLE_1, ZONE_1) >> Optional.of(VALUE)
		1 * zoneVariableManager.setValue(ZONE_VARIABLE_2, ZONE_2, VALUE)
	}
}
