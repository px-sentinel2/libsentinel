package net.poundex.sentinel2.server.env.action

import net.poundex.sentinel2.server.SentinelEnvironment
import spock.lang.Specification
import spock.lang.Subject

class MacroActionSpec extends Specification {

	Action action1 = Mock()
	Action action2 = Mock()
	SentinelEnvironment sentinelEnvironment = Stub()
	
	@Subject
	MacroAction action =
			new MacroAction("id", "name", [action1, action2])

	void "Runs all child actions"() {
		when:
		action.run(sentinelEnvironment)

		then:
		1 * action1.run(sentinelEnvironment)
		1 * action2.run(sentinelEnvironment)
	}
}
