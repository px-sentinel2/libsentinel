package net.poundex.sentinel2.server.env.zonevariable

import net.poundex.sentinel2.server.env.value.BooleanValue
import net.poundex.sentinel2.server.env.value.ValueWithMetadata
import spock.lang.Specification

import java.time.Instant

import static net.poundex.sentinel2.server.env.value.BooleanValue.FALSE
import static net.poundex.sentinel2.server.env.value.BooleanValue.TRUE

class BooleanFoldStrategySpec extends Specification {
	void "Any of folds zone variables"(Collection<ValueWithMetadata<BooleanValue>> input, BooleanValue expected) {
		expect:
		BooleanZoneVariable.BooleanFoldStrategy.ANY_OF.fold(input) == expected

		where:
		input || expected
		withMetadata([FALSE, FALSE, FALSE]) || FALSE
		withMetadata([FALSE, FALSE, TRUE])  || TRUE
		withMetadata([TRUE, TRUE, TRUE])    || TRUE
		withMetadata([])                    || FALSE
	}
	
	private static List<ValueWithMetadata<BooleanValue>> withMetadata(List<BooleanValue> values) {
		return values.collect { new ValueWithMetadata<BooleanValue>(it, Instant.now())}
	}
}
