package net.poundex.sentinel2.server.env.action

import net.poundex.sentinel2.server.SentinelEnvironment
import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.appliance.Appliance
import net.poundex.sentinel2.server.appliance.ApplianceRepository
import net.poundex.sentinel2.server.appliance.ApplianceRole
import net.poundex.sentinel2.server.device.Device
import net.poundex.sentinel2.server.device.DeviceManager
import net.poundex.sentinel2.server.device.port.DevicePort
import net.poundex.sentinel2.server.device.port.WritableDevicePort
import net.poundex.sentinel2.server.env.action.SetSceneActivationAction.Activation
import net.poundex.sentinel2.server.env.scene.Scene
import net.poundex.sentinel2.server.env.value.BooleanValue
import net.poundex.sentinel2.server.env.value.ColourValue
import net.poundex.sentinel2.server.env.value.SceneValue
import net.poundex.sentinel2.server.env.zonevariable.SceneZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableManager
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification
import spock.lang.Subject

class SetSceneActivationActionSpec extends Specification {
		
	private static final Zone ZONE = Zone.builder().build()
	private static final ColourValue COLOUR = new ColourValue(1, 2, 3)
	private static final Scene SCENE = Scene.builder().values(List.of(COLOUR)).build()
	private static final SceneValue SCENE_VALUE = new SceneValue(SCENE)
	private static final SceneZoneVariable SZV = SceneZoneVariable.builder().name("scene").build()
	
	private static final Appliance COLOUR_LIGHT_1 = Appliance.builder()
			.zone(ZONE)
			.roles([ApplianceRole.SCENE, ApplianceRole.LIGHT, ApplianceRole.COLOUR].toSet())
			.deviceId(URI.create("dev://dev1/self"))
			.build()
	private static final Appliance COLOUR_LIGHT_2 = Appliance.builder()
			.zone(ZONE)
			.roles([ApplianceRole.SCENE, ApplianceRole.LIGHT, ApplianceRole.COLOUR].toSet())
			.deviceId(URI.create("dev://dev2/self"))
			.build()
	private static final Appliance COLOUR_LIGHT_3 = Appliance.builder()
			.zone(ZONE)
			.roles([ApplianceRole.SCENE, ApplianceRole.LIGHT, ApplianceRole.COLOUR].toSet())
			.deviceId(URI.create("dev://dev3/self"))
			.build()
	
	private static final Appliance APPLIANCE_1 = Appliance.builder()
			.zone(ZONE)
			.roles([ApplianceRole.SCENE, ApplianceRole.LIGHT].toSet())
			.deviceId(URI.create("dev://dev4/self"))
			.build()
	private static final Appliance APPLIANCE_2  = Appliance.builder()
			.zone(ZONE)
			.roles([ApplianceRole.SCENE].toSet())
			.deviceId(URI.create("dev://dev5/self"))
			.build()
	private static final Appliance APPLIANCE_3 = Appliance.builder()
			.zone(ZONE)
			.roles([ApplianceRole.SCENE].toSet())
			.deviceId(URI.create("dev://dev6/self"))
			.build()
	
	WritableDevicePort app1PowerPort = Mock()
	WritableDevicePort app2PowerPort = Mock()

	WritableDevicePort colourLight1PowerPort = Mock()
	WritableDevicePort colourLight1ColourPort = Mock()
	
	WritableDevicePort colourLight2PowerPort = Mock()
	WritableDevicePort colourLight2ColourPort = Mock()

	ApplianceRepository applianceRepository = Stub() {
		findAllByZoneWithRoles(ZONE, [ApplianceRole.SCENE].toSet()) >>
				Flux.just(
						APPLIANCE_1,
						APPLIANCE_2,
						APPLIANCE_3,
						COLOUR_LIGHT_1,
						COLOUR_LIGHT_2,
						COLOUR_LIGHT_3)
	}

	DeviceManager deviceManager = Stub() {
		getDevice(APPLIANCE_1.getDeviceId()) >> Optional.of(Stub(Device) {
			getPort(DevicePort.POWER) >> Optional.of(app1PowerPort)
		})
		getDevice(APPLIANCE_2.getDeviceId()) >> Optional.of(Stub(Device) {
			getPort(DevicePort.POWER) >> Optional.of(app2PowerPort)
		})

		getDevice(COLOUR_LIGHT_1.getDeviceId()) >> Optional.of(Stub(Device) {
			getPort(DevicePort.POWER) >> Optional.of(colourLight1PowerPort)
			getPort(DevicePort.COLOUR) >> Optional.of(colourLight1ColourPort)
		})
		getDevice(COLOUR_LIGHT_2.getDeviceId()) >> Optional.of(Stub(Device) {
			getPort(DevicePort.POWER) >> Optional.of(colourLight2PowerPort)
			getPort(DevicePort.COLOUR) >> Optional.of(colourLight2ColourPort)
		})
	}

	ZoneVariableRepository zoneVariableRepository = Stub() {
		findByName("scene") >> Mono.just(SZV)
	}

	ZoneVariableManager zoneVariableManager = Stub() {
		getValue(SZV, ZONE) >> Optional.of(SCENE_VALUE)
	}

	SentinelEnvironment sentinelEnvironment = Stub() {
		getApplianceRepository() >> applianceRepository
		getDeviceManager() >> deviceManager
		getZoneVariableRepository() >> zoneVariableRepository
		getZoneVariableManager() >> zoneVariableManager
	}
	
	void "Activates scene by setting all colour lights to scene and turning all other appliances on"() {
		given:
		@Subject
		SetSceneActivationAction action = new SetSceneActivationAction(null, null, ZONE, Activation.ACTIVATE)
		
		when:
		action.run(sentinelEnvironment)
		
		then:
		1 * app1PowerPort.writeValue(BooleanValue.TRUE)
		1 * app2PowerPort.writeValue(BooleanValue.TRUE)
		
		and:
		1 * colourLight1ColourPort.writeValue(COLOUR)
		1 * colourLight2ColourPort.writeValue(COLOUR)
	}
	
	void "Deactivates scene by turning all appliances off"() {
		given:
		@Subject
		SetSceneActivationAction action = new SetSceneActivationAction(null, null, ZONE, Activation.DEACTIVATE)

		when:
		action.run(sentinelEnvironment)

		then:
		1 * app1PowerPort.writeValue(BooleanValue.FALSE)
		1 * app2PowerPort.writeValue(BooleanValue.FALSE)
		1 * colourLight1PowerPort.writeValue(BooleanValue.FALSE)
		1 * colourLight2PowerPort.writeValue(BooleanValue.FALSE)
	}
}
