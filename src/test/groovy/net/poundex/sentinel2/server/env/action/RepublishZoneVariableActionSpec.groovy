package net.poundex.sentinel2.server.env.action

import net.poundex.sentinel2.server.SentinelEnvironment
import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.value.BooleanValue
import net.poundex.sentinel2.server.env.zonevariable.BooleanZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableManager
import spock.lang.Specification
import spock.lang.Subject

class RepublishZoneVariableActionSpec extends Specification {

	private static final Zone ZONE = Zone.builder().build()
	private static final ZoneVariable ZONE_VARIABLE = BooleanZoneVariable.builder().build()

	ZoneVariableManager zoneVariableManager = Mock()

	SentinelEnvironment sentinelEnvironment = Stub() {
		getZoneVariableManager() >> zoneVariableManager
	}
	
	@Subject
	RepublishZoneVariableAction action = new RepublishZoneVariableAction(null, null, ZONE, ZONE_VARIABLE)
	
	void "Republishes zone variable value"() {
		when:
		action.run(sentinelEnvironment)

		then:
		1 * zoneVariableManager.getValue(ZONE_VARIABLE, ZONE) >> Optional.empty()
		
		when:
		action.run(sentinelEnvironment)
		
		then:
		1 * zoneVariableManager.getValue(ZONE_VARIABLE, ZONE) >> Optional.of(BooleanValue.TRUE)
		1 * zoneVariableManager.setValue(ZONE_VARIABLE, ZONE, BooleanValue.TRUE)
	}
}
