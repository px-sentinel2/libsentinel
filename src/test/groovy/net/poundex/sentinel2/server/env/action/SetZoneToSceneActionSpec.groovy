package net.poundex.sentinel2.server.env.action

import net.poundex.sentinel2.server.SentinelEnvironment
import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.appliance.Appliance
import net.poundex.sentinel2.server.appliance.ApplianceRepository
import net.poundex.sentinel2.server.appliance.ApplianceRole
import net.poundex.sentinel2.server.device.Device
import net.poundex.sentinel2.server.device.DeviceManager
import net.poundex.sentinel2.server.device.port.DevicePort
import net.poundex.sentinel2.server.device.port.WritableDevicePort
import net.poundex.sentinel2.server.env.scene.Scene
import net.poundex.sentinel2.server.env.value.ColourValue
import reactor.core.publisher.Flux
import spock.lang.Specification
import spock.lang.Subject

class SetZoneToSceneActionSpec extends Specification {
	
	private static final Zone ZONE = Zone.builder().build()
	private static final ColourValue VALUE1 = new ColourValue(1, 2, 3)
	private static final ColourValue VALUE2 = new ColourValue(1, 2, 4)
	private static final Scene SCENE = Scene.builder().values([VALUE1, VALUE2]).build()
	
	private static final Appliance COLOUR_LIGHT_1 = Appliance.builder()
			.zone(ZONE)
			.roles([ApplianceRole.SCENE, ApplianceRole.LIGHT, ApplianceRole.COLOUR].toSet())
			.deviceId(URI.create("dev://dev1/self"))
			.build()
	private static final Appliance COLOUR_LIGHT_2 = Appliance.builder()
			.zone(ZONE)
			.roles([ApplianceRole.SCENE, ApplianceRole.LIGHT, ApplianceRole.COLOUR].toSet())
			.deviceId(URI.create("dev://dev2/self"))
			.build()
	private static final Appliance COLOUR_LIGHT_3 = Appliance.builder()
			.zone(ZONE)
			.roles([ApplianceRole.SCENE, ApplianceRole.LIGHT, ApplianceRole.COLOUR].toSet())
			.deviceId(URI.create("dev://dev3/self"))
			.build()

	WritableDevicePort port1 = Mock()
	WritableDevicePort port2 = Mock()
	WritableDevicePort port3 = Mock()

	ApplianceRepository applianceRepository = Stub() {
		findAllByZoneWithRoles(ZONE, [ApplianceRole.SCENE, ApplianceRole.COLOUR, ApplianceRole.LIGHT].toSet()) >>
				Flux.just(COLOUR_LIGHT_1, COLOUR_LIGHT_2, COLOUR_LIGHT_3)
	}

	DeviceManager deviceManager = Stub() {
		getDevice(COLOUR_LIGHT_1.getDeviceId()) >> Optional.of(Stub(Device) {
			getPort(DevicePort.COLOUR) >> Optional.of(port1) 
		})
		getDevice(COLOUR_LIGHT_2.getDeviceId()) >> Optional.of(Stub(Device) {
			getPort(DevicePort.COLOUR) >> Optional.of(port2)
		})
		getDevice(COLOUR_LIGHT_3.getDeviceId()) >> Optional.of(Stub(Device) {
			getPort(DevicePort.COLOUR) >> Optional.of(port3)
		})
	}

	SentinelEnvironment sentinelEnvironment = Stub() {
		getApplianceRepository() >> applianceRepository
		getDeviceManager() >> deviceManager
	}
	
	@Subject
	SetZoneToSceneAction action = new SetZoneToSceneAction(null, null, ZONE, SCENE)
	
	void "Write scene values to all appliances"() {
		when:
		action.run(sentinelEnvironment)
		
		then:
		1 * port1.writeValue(VALUE1)
		1 * port2.writeValue(VALUE2)
		1 * port3.writeValue(VALUE1)
	}
}
