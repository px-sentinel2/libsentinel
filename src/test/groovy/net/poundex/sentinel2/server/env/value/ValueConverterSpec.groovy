package net.poundex.sentinel2.server.env.value

import com.fasterxml.jackson.databind.ObjectMapper
import groovy.transform.Immutable
import spock.lang.Specification
import spock.lang.Subject

class ValueConverterSpec extends Specification {
	
	@Subject
	ValueConverter valueConverter = new ValueConverter(new ObjectMapper())

	@Immutable
	static class TestValue implements Value<TestValue> {
		String foo
		int bar
		boolean baz
	}
	
	void "Read values"() {
		expect:
		valueConverter.readRaw("""${TestValue.class.name} {"bar": 123, "foo": "foofoo", "baz": true}""") == 
				new TestValue("foofoo", 123, true)
	}
	
	void "Write values"() {
		when:
		String raw = valueConverter.writeRaw(new TestValue("foofoo", 123, true))
		
		then:
		raw.readLines().size() == 1
		valueConverter.readRaw(raw) == new TestValue("foofoo", 123, true)
	}
}
