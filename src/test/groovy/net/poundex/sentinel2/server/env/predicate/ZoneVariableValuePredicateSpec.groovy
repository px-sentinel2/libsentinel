package net.poundex.sentinel2.server.env.predicate

import net.poundex.sentinel2.server.SentinelEnvironment
import net.poundex.sentinel2.server.Zone
import net.poundex.sentinel2.server.env.value.Value
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableManager
import spock.lang.Shared
import spock.lang.Specification

class ZoneVariableValuePredicateSpec extends Specification {
	
	static final Zone ZONE = Zone.builder().build()
	
	ZoneVariable zoneVariable = Stub()
	@Shared
	Value value1 = Stub()
	@Shared
	Value value2 = Stub()
	SentinelEnvironment environment = Stub() {
		getZoneVariableManager() >> Stub(ZoneVariableManager) {
			getValue(zoneVariable, ZONE) >> Optional.of(value1)
		}
	}
	
	void "Returns correct value by looking up zone variable value"(Value value, boolean expected) {
		given:
		ZoneVariableValuePredicate predicate =
				new ZoneVariableValuePredicate(null, null, ZONE, zoneVariable, value)

		expect:
		predicate.test(environment) == expected

		where:
		value  || expected
		value1 || true
		value2 || false
	}
}
