package net.poundex.sentinel2.server;

import lombok.*;
import net.poundex.sentinel2.SentinelObject;

import java.util.Optional;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@ToString(onlyExplicitlyIncluded = true)
public class Zone implements SentinelObject {
	@ToString.Include
	private final String id;
	@ToString.Include
	private final String name;
	private final Optional<Zone> parent;
}
