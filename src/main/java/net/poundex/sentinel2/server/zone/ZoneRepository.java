package net.poundex.sentinel2.server.zone;

import net.poundex.sentinel2.ReadOnlyRepository;
import net.poundex.sentinel2.server.Zone;
import reactor.core.publisher.Mono;

public interface ZoneRepository extends ReadOnlyRepository<Zone> {
	Mono<Zone> create(String name, Zone parent);
}
