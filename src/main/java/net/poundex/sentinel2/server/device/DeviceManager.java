package net.poundex.sentinel2.server.device;

import java.net.URI;
import java.util.Optional;

public interface DeviceManager {
	Optional<Device> getDevice(URI deviceId);
}
