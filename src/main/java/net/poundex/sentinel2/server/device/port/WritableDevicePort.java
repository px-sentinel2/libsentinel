package net.poundex.sentinel2.server.device.port;

import net.poundex.sentinel2.server.env.value.Value;
import org.slf4j.LoggerFactory;

public interface WritableDevicePort<VT extends Value<VT>> extends DevicePort<VT> {
	
	static <VT extends Value<VT>> void write(DevicePort<VT> devicePort, VT value) {
		if(devicePort instanceof WritableDevicePort<VT> wdp)
			doWrite(wdp, value);
		else
			throw new PortNotWritableException(devicePort.getPortId(), value);
	}
	
	private static <VT extends Value<VT>> void doWrite(WritableDevicePort<VT> devicePort, VT value) {
		LoggerFactory.getLogger(WritableDevicePort.class).debug("Writing {} to {}", value, devicePort);
		devicePort.writeValue(value);
	}
	
	void writeValue(VT value);
}
