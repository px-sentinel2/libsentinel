package net.poundex.sentinel2.server.device;

import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.env.value.Value;

import java.net.URI;
import java.util.Optional;

public interface Device {
	URI getDeviceId();
	
	<VT extends Value<VT>> Optional<DevicePort<VT>> getPort(URI path);
}
