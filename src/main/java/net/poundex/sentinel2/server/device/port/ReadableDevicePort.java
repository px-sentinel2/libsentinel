package net.poundex.sentinel2.server.device.port;

import net.poundex.sentinel2.server.env.value.Value;

public interface ReadableDevicePort<VT extends Value<VT>> extends DevicePort<VT> {
	VT readValue();
}
