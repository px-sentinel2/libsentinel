package net.poundex.sentinel2.server.device;

public interface Driver {
	String getName();
	void start();
}
