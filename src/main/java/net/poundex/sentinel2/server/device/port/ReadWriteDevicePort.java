package net.poundex.sentinel2.server.device.port;

import net.poundex.sentinel2.server.env.value.Value;

public interface ReadWriteDevicePort<VT extends Value<VT>> extends ReadableDevicePort<VT>, WritableDevicePort<VT> {
}
