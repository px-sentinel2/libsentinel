package net.poundex.sentinel2.server.device.port;

import java.net.URI;

public class PortNotWritableException extends RuntimeException {
	public PortNotWritableException(URI portId, Object value) {
		super(String.format("Could not write %s to non-writable port %s", value, portId));
	}
}
