package net.poundex.sentinel2.server.device.port;


import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.messaging.Subscribable;

public interface PortEvents {
	
	record PortValuePublishedEvent<VT extends Value<VT>>(DevicePort<VT> port, Value<VT> value) { }
	
	<VT extends Value<VT>> void portValuePublished(PortValuePublishedEvent<VT> event);
	<VT extends Value<VT>> Subscribable<PortValuePublishedEvent<VT>> portValuePublished();
}
