package net.poundex.sentinel2.server.device;

import net.poundex.sentinel2.server.messaging.Subscribable;

public interface DriverEvents {
	void deviceAdded(Device device);
	Subscribable<Device> deviceAdded();
}
