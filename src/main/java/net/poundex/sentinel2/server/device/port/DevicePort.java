package net.poundex.sentinel2.server.device.port;

import lombok.*;
import net.poundex.sentinel2.server.env.value.Value;

import java.net.URI;

public interface DevicePort<VT extends Value<VT>> {
	URI POWER = URI.create("power");
	URI COLOUR = URI.create("colour");
	URI COLOUR_TEMPERATURE = URI.create("ct");
	URI TEMPERATURE = URI.create("temperature");
	URI HUMIDITY = URI.create("humidity");
	URI LUMINANCE = URI.create("luminance");
	URI MOTION = URI.create("motion");
	URI TAMPER = URI.create("tamper");
//	URI BATTERY_LEVEL = URI.create("batterylevel");
	
	URI getPortId();


	@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
	@ToString(onlyExplicitlyIncluded = true)
	@EqualsAndHashCode(onlyExplicitlyIncluded = true)
	class SimpleDevicePort<VT extends Value<VT>> implements DevicePort<VT> {
		@Getter
		@ToString.Include
		@EqualsAndHashCode.Include
		private final URI portId;
	}
	
	static <VT extends Value<VT>> SimpleDevicePort<VT> simple(URI portId) {
		return new SimpleDevicePort<>(portId);
	}
}
