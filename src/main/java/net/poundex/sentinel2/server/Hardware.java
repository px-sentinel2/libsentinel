package net.poundex.sentinel2.server;

import java.net.URI;

public interface Hardware {
	URI getDeviceId();
	String getName();
}
