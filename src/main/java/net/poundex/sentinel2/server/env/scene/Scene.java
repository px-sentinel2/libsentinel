package net.poundex.sentinel2.server.env.scene;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.env.value.ColourValue;

import java.util.List;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
public class Scene implements SentinelObject {
	private final String id;
	private final String name;
	private final List<ColourValue> values;
}
	