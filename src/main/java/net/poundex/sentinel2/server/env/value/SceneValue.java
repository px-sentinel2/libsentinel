package net.poundex.sentinel2.server.env.value;

import net.poundex.sentinel2.server.env.scene.Scene;

public record SceneValue(Scene value) implements Value<SceneValue> {
}
