package net.poundex.sentinel2.server.env.zonevariable.trigger;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.action.Action;
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;

import java.util.Optional;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
public class ZoneVariableTrigger<VT extends Value<VT>, T extends ZoneVariable<VT>> implements SentinelObject {
	
	private final String id;
	private final String name;
	private final Zone zone;
	private final T zoneVariable;
	private final VT value;
	private final Action action;
	private final Optional<EnvironmentPredicate> predicate;
}
