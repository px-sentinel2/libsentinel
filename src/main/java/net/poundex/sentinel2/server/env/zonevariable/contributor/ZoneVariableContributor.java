package net.poundex.sentinel2.server.env.zonevariable.contributor;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;

import java.net.URI;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
public class ZoneVariableContributor implements SentinelObject {
	
	private final String id;
	private final ZoneVariable<?> zoneVariable;
	private final Zone zone;
	private final URI portId;
}
