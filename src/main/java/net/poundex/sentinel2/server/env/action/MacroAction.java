package net.poundex.sentinel2.server.env.action;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.SentinelEnvironment;

import java.util.List;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Slf4j
@ToString(onlyExplicitlyIncluded = true)
public class MacroAction implements Action, SentinelObject {
	@ToString.Include
	private final String id;
	@ToString.Include
	private final String name;
	private final List<? extends Action> actions;
	
	@Override
	public void run(SentinelEnvironment environment) {
		actions.forEach(a -> {
			log.info("Running action: {}", a);
			a.run(environment);
		});
	}
}
