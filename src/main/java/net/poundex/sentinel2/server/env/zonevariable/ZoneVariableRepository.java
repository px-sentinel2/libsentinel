package net.poundex.sentinel2.server.env.zonevariable;

import net.poundex.sentinel2.ReadOnlyRepository;
import net.poundex.sentinel2.server.env.value.BooleanValue;
import net.poundex.sentinel2.server.env.value.NumericValue;
import net.poundex.sentinel2.server.env.value.SceneValue;
import reactor.core.publisher.Mono;

public interface ZoneVariableRepository extends ReadOnlyRepository<ZoneVariable<?>> {
	Mono<NumericZoneVariable> createNumeric(
			String name, 
			NumericZoneVariable.NumericFoldStrategy foldStrategy, 
			NumericValue defaultValue);
	
	Mono<BooleanZoneVariable> createBool(
			String name,
			BooleanZoneVariable.BooleanFoldStrategy foldStrategy,
			BooleanValue defaultValue);
	
	Mono<SceneZoneVariable> createScene(
			String name, 
			SceneValue defaultValue);
	
	<T extends ZoneVariable<?>> Mono<T> findByName(String name);
}
