package net.poundex.sentinel2.server.env.zonevariable;

import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.value.ValueWithMetadata;

import java.util.Collection;

public interface FoldStrategy<VT extends Value<VT>> {
	VT fold(Collection<ValueWithMetadata<VT>> values);
}
