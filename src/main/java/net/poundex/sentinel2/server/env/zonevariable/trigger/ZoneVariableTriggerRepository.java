package net.poundex.sentinel2.server.env.zonevariable.trigger;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.action.Action;
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ZoneVariableTriggerRepository {
	<VT extends Value<VT>, T extends ZoneVariable<VT>> Mono<ZoneVariableTrigger<VT, T>> create(
			String name,
			Zone zone,
			T zoneVariable,
			VT value,
			Action action,
			EnvironmentPredicate environmentPredicate);

	<VT extends Value<VT>, T extends ZoneVariable<VT>> Flux<ZoneVariableTrigger<VT, T>> findByZoneAndZoneVariable(Zone zone, T zoneVariable);
}
