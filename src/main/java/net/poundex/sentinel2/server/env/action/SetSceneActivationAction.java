package net.poundex.sentinel2.server.env.action;

import lombok.*;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.SentinelEnvironment;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.appliance.Appliance;
import net.poundex.sentinel2.server.appliance.ApplianceRole;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.device.port.WritableDevicePort;
import net.poundex.sentinel2.server.env.value.BooleanValue;
import net.poundex.sentinel2.server.env.value.ColourValue;
import net.poundex.sentinel2.server.env.value.SceneValue;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.SceneZoneVariable;
import org.apache.commons.collections4.IteratorUtils;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Slf4j
public class SetSceneActivationAction implements Action, SentinelObject {

	private static final Set<ApplianceRole> SCENE_ROLE = Set.of(ApplianceRole.SCENE);
	
	private final String id;
	private final String name;
	private final Zone zone;
	private final Activation activation;

	@Override
	public void run(SentinelEnvironment environment) {
		environment.getApplianceRepository()
				.findAllByZoneWithRoles(zone, SCENE_ROLE)
				.collectList()
				.subscribe(l -> this.runForAppliances(l, environment));
	}

	private void runForAppliances(List<Appliance> sceneAppliances, SentinelEnvironment environment) {
		if (activation == Activation.ACTIVATE)
			activateScene(sceneAppliances, environment);
		else
			sceneAppliances.forEach(app -> 
					writeDevicePortValue(environment, app.getDeviceId(), DevicePort.POWER, () -> BooleanValue.FALSE));
	}

	private void activateScene(List<Appliance> sceneAppliances, SentinelEnvironment environment) {
		environment.getZoneVariableRepository().<SceneZoneVariable>findByName("scene")
				.flatMap(szv -> Mono.justOrEmpty(environment.getZoneVariableManager().getValue(szv, zone)))
				.map(SceneValue::value)
				.subscribe(scene -> {
					Iterator<ColourValue> colours = IteratorUtils.loopingIterator(scene.getValues());
					sceneAppliances.forEach(app -> {
						if (app.getRoles().containsAll(Set.of(ApplianceRole.COLOUR, ApplianceRole.LIGHT)))
							writeDevicePortValue(environment, app.getDeviceId(), DevicePort.COLOUR, colours::next);
						else
							writeDevicePortValue(environment, app.getDeviceId(), DevicePort.POWER, () -> BooleanValue.TRUE);
					});
				});
	}

	private <VT extends Value<VT>> void writeDevicePortValue(
			SentinelEnvironment environment,
			URI deviceId,
			URI portPath,
			Supplier<VT> value) {
		environment.getDeviceManager().getDevice(deviceId)
				.flatMap(d -> d.<VT>getPort(portPath))
				.ifPresent(dp -> WritableDevicePort.write(dp, value.get()));
	}

	@RequiredArgsConstructor
	public enum Activation {
		ACTIVATE(BooleanValue.TRUE), 
		DEACTIVATE(BooleanValue.FALSE);
		
		@Getter
		private final BooleanValue value;
	}
}
