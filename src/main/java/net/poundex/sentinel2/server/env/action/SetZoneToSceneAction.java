package net.poundex.sentinel2.server.env.action;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.SentinelEnvironment;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.appliance.ApplianceRole;
import net.poundex.sentinel2.server.device.port.DevicePort;
import net.poundex.sentinel2.server.device.port.WritableDevicePort;
import net.poundex.sentinel2.server.env.scene.Scene;
import net.poundex.sentinel2.server.env.value.ColourValue;
import org.apache.commons.collections4.IterableUtils;

import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Slf4j
public class SetZoneToSceneAction implements Action, SentinelObject {
	
	private static final Set<ApplianceRole> SCENE_COLOUR_LIGHTS =
			EnumSet.of(ApplianceRole.SCENE, ApplianceRole.COLOUR, ApplianceRole.LIGHT);
	
	private final String id;
	private final String name;
	private final Zone zone;
	private final Scene scene;
	
	@Override
	public void run(SentinelEnvironment environment) {
		record ValueAndPort(ColourValue value, Optional<DevicePort<ColourValue>> port) { }

		environment.getApplianceRepository()
				.findAllByZoneWithRoles(zone, SCENE_COLOUR_LIGHTS)
				.map(app -> environment.getDeviceManager().getDevice(app.getDeviceId()))
				.map(d -> d.flatMap(d2 -> d2.<ColourValue>getPort(DevicePort.COLOUR)))
				.zipWithIterable(IterableUtils.loopingIterable(scene.getValues()),
						(port, value) -> new ValueAndPort(value, port))
				.subscribe(vp -> vp.port().ifPresent(p -> WritableDevicePort.write(p, vp.value())));
	}
}
