package net.poundex.sentinel2.server.env.action;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.SentinelEnvironment;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Slf4j
public class RepublishZoneVariableAction implements Action, SentinelObject {

	private final String id;
	private final String name;
	private final Zone zone;
	private final ZoneVariable<?> zoneVariable;

	@Override
	public void run(SentinelEnvironment environment) {
		doRun(environment);
	}

	@SuppressWarnings("unchecked")
	private <VT extends Value<VT>> void doRun(SentinelEnvironment environment) {
		environment.getZoneVariableManager()
				.getValue((ZoneVariable<VT>) zoneVariable, zone)
				.ifPresent(v -> environment.getZoneVariableManager()
						.setValue((ZoneVariable<VT>) zoneVariable, zone, v));
	}
}
