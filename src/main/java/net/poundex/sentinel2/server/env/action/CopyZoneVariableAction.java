package net.poundex.sentinel2.server.env.action;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.SentinelEnvironment;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Slf4j
public class CopyZoneVariableAction<VT extends Value<VT>> implements Action, SentinelObject {
	
	private final String id;
	private final String name;
	private final Zone sourceZone;
	private final ZoneVariable<VT> source;
	private final Zone destinationZone;
	private final ZoneVariable<VT> destination;

	@Override
	public void run(SentinelEnvironment environment) {
		environment.getZoneVariableManager().getValue(source, sourceZone)
				.ifPresent(v -> environment.getZoneVariableManager().setValue(destination, destinationZone, v));
	}
}
