package net.poundex.sentinel2.server.env.predicate;

import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.SentinelEnvironment;

public interface EnvironmentPredicate extends SentinelObject {
	boolean test(SentinelEnvironment environment);
}
