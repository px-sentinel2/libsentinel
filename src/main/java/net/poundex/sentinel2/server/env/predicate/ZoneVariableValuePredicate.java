package net.poundex.sentinel2.server.env.predicate;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import net.poundex.sentinel2.server.SentinelEnvironment;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;

@RequiredArgsConstructor
@Data
@Builder
public class ZoneVariableValuePredicate<VT extends Value<VT>> implements EnvironmentPredicate {
	
	private final String id;
	private final String name;
	private final Zone zone;
	private final ZoneVariable<VT> zoneVariable;
	private final VT value;
	
	@Override
	public boolean test(SentinelEnvironment environment) {
		return environment.getZoneVariableManager().getValue(zoneVariable, zone)
				.map(v -> v.equals(value))
				.orElse(false);
	}
}
