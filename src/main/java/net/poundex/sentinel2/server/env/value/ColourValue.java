package net.poundex.sentinel2.server.env.value;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ColourValue implements Value<ColourValue> {
	
	private final int brightness;
	private final int hue;
	private final int saturation;
}
