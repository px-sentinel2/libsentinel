package net.poundex.sentinel2.server.env.value;

public record NumericValue(double value) implements Value<NumericValue> { }
