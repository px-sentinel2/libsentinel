package net.poundex.sentinel2.server.env.zonevariable;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.env.value.BooleanValue;
import net.poundex.sentinel2.server.env.value.ValueWithMetadata;

import java.util.Collection;
import java.util.function.Function;

@Getter
@SuperBuilder
public class BooleanZoneVariable extends ZoneVariable<BooleanValue> {
	
	private final BooleanFoldStrategy zoneFoldStrategy;

	@Override
	public BooleanValue fold(Collection<ValueWithMetadata<BooleanValue>> values) {
		return zoneFoldStrategy.zoneFn.apply(values);
	}

	@RequiredArgsConstructor
	public enum BooleanFoldStrategy implements FoldStrategy<BooleanValue> {
		
		ANY_OF(vs -> 
				new BooleanValue(vs.stream()
						.map(ValueWithMetadata::value)
						.anyMatch(BooleanValue::value)));
		
		private final Function<Collection<ValueWithMetadata<BooleanValue>>, BooleanValue> zoneFn;
		
		@Override
		public BooleanValue fold(Collection<ValueWithMetadata<BooleanValue>> values) {
			return zoneFn.apply(values);
		}
	}
}
