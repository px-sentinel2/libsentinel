package net.poundex.sentinel2.server.env.value;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ColourTemperatureValue implements Value<ColourTemperatureValue> {
	
	private final int brightness;
	private final int temperature;

}
