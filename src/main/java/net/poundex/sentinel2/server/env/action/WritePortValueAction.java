package net.poundex.sentinel2.server.env.action;

import io.vavr.control.Try;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.SentinelEnvironment;
import net.poundex.sentinel2.server.appliance.Appliance;
import net.poundex.sentinel2.server.device.port.PortNotWritableException;
import net.poundex.sentinel2.server.device.port.WritableDevicePort;
import net.poundex.sentinel2.server.env.value.Value;

import java.net.URI;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Slf4j
public class WritePortValueAction<VT extends Value<VT>> implements Action, SentinelObject {
	private final String id;
	private final String name;
	private final Appliance appliance;
	private final URI portPath;
	private final VT value;
	
	@Override
	public void run(SentinelEnvironment environment) {
		environment.getDeviceManager().getDevice(appliance.getDeviceId())
				.flatMap(d -> d.<VT>getPort(portPath))
				.ifPresentOrElse(
						dp -> Try.run(() -> WritableDevicePort.write(dp, value))
								.onFailure(PortNotWritableException.class,
										x -> log.error(x.getMessage())),
						() -> log.error("Could not find port {} on device {}",
								portPath, appliance.getDeviceId()));
	}
}
