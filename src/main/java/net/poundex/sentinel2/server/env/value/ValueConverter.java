package net.poundex.sentinel2.server.env.value;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
public class ValueConverter {

	private final ObjectMapper objectMapper;
	
	@SuppressWarnings("unchecked")
	public <VT extends Value<VT>> VT readRaw(String rawValue) {
		if( ! StringUtils.hasText(rawValue))
			return null;
		
		String[] bits = rawValue.split(" ", 2);
		return Try.of(() -> Class.forName(bits[0]))
				.mapTry(klass -> objectMapper.readValue(bits[1], klass))
				.map(v -> (VT) v)
				.get();
	}

	public String writeRaw(Value<?> value) {
		if(value == null)
			return null;
		
		return value.getClass().getName() + " " + Try.of(() -> objectMapper.writeValueAsString(value)).get();
	}
}
