package net.poundex.sentinel2.server.env.zonevariable;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.messaging.Subscribable;

public interface ZoneVariableEvents {
	record ZoneVariableUpdatedEvent<VT extends Value<VT>>(
			ZoneVariable<VT> zoneVariable,
			Zone zone, 
			Value<VT> value) { }
	
	<VT extends Value<VT>> void zoneVariableUpdated(ZoneVariableUpdatedEvent<VT> event);
	<VT extends Value<VT>> Subscribable<ZoneVariableUpdatedEvent<VT>> zoneVariableUpdated();
	
}
