package net.poundex.sentinel2.server.env.zonevariable.contributor;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;

public interface ZoneVariableContributorRepository {
	Mono<ZoneVariableContributor> create(ZoneVariable<?> zoneVariable, Zone zone, URI portId);
	Flux<ZoneVariableContributor> findByPortId(URI portId);
}
