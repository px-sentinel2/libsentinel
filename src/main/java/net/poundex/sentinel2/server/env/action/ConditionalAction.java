package net.poundex.sentinel2.server.env.action;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.SentinelEnvironment;
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Slf4j
public class ConditionalAction implements Action, SentinelObject {

	private final String id;
	private final String name;
	private final EnvironmentPredicate predicate;
	private final Action action;

	@Override
	public void run(SentinelEnvironment environment) {
		if(predicate.test(environment))
			action.run(environment);
	}
}
