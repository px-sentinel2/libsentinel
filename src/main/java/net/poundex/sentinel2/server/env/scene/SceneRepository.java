package net.poundex.sentinel2.server.env.scene;

import net.poundex.sentinel2.server.env.value.ColourValue;
import reactor.core.publisher.Mono;

public interface SceneRepository {
	Mono<Scene> create(String name, ColourValue... colourValues);
}
