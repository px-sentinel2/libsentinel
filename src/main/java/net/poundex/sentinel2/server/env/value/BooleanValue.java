package net.poundex.sentinel2.server.env.value;

public record BooleanValue(boolean value) implements Value<BooleanValue> {

	public static final BooleanValue TRUE = new BooleanValue(true);
	public static final BooleanValue FALSE = new BooleanValue(false);
	
	public BooleanValue or(BooleanValue other) {
		return new BooleanValue(value || other.value());
	}

}
