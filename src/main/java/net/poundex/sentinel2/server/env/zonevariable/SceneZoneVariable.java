package net.poundex.sentinel2.server.env.zonevariable;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.env.value.SceneValue;
import net.poundex.sentinel2.server.env.value.ValueWithMetadata;

import java.util.Collection;

@Getter
@SuperBuilder
public class SceneZoneVariable extends ZoneVariable<SceneValue> {
	
	@Override
	public SceneValue fold(Collection<ValueWithMetadata<SceneValue>> values) {
		throw new UnsupportedOperationException();
	}
}
