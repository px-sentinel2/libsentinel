package net.poundex.sentinel2.server.env.action;

public interface ActionManager {
	void run(Action action);
}
