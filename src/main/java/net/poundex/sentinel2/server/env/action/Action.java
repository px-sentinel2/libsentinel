package net.poundex.sentinel2.server.env.action;

import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.SentinelEnvironment;

public interface Action extends SentinelObject {
	String getName();
	void run(SentinelEnvironment environment);
}
