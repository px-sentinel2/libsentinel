package net.poundex.sentinel2.server.env.action;

import net.poundex.sentinel2.ReadOnlyRepository;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.appliance.Appliance;
import net.poundex.sentinel2.server.env.predicate.EnvironmentPredicate;
import net.poundex.sentinel2.server.env.scene.Scene;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.List;

public interface ActionRepository extends ReadOnlyRepository<Action> {
	<VT extends Value<VT>> Mono<WritePortValueAction<VT>> createWritePortValueAction(
			String name, Appliance appliance, URI portPath, VT value);
	
	Mono<MacroAction> createMacroAction(String name, List<? extends Action> actions);
	
	Mono<SetZoneToSceneAction> createSetZoneToScene(
			String name, Zone zone, Scene scene);

	Mono<SetSceneActivationAction> createSetSceneActivation(
			String name, Zone zone, SetSceneActivationAction.Activation activation);
	
	<VT extends Value<VT>> Mono<SetZoneVariableAction<VT>> createSetZoneVariable(
			String name, Zone zone, ZoneVariable<VT> zoneVariable, VT value);
	
	Mono<RepublishZoneVariableAction> createRepublishZoneVariable(
			String name, Zone zone, ZoneVariable<?> zoneVariable);
	
	Mono<ConditionalAction> createConditional(
			String name, EnvironmentPredicate predicate, Action action);
	
	Mono<RunFirstConditionalActionAction> createRunFirstConditionalAction(
			String name, List<ConditionalAction> actions);
	
	<VT extends Value<VT>> Mono<CopyZoneVariableAction<VT>> createCopyZoneVariableAction(
			String name, 
			Zone sourceZone,
			ZoneVariable<VT> source,
			Zone destinationZone,
			ZoneVariable<VT> destination);
}
