package net.poundex.sentinel2.server.env.predicate;

import net.poundex.sentinel2.ReadOnlyRepository;
import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariable;
import reactor.core.publisher.Mono;

public interface EnvironmentPredicateRepository extends ReadOnlyRepository<EnvironmentPredicate> {
	
	<VT extends Value<VT>> Mono<ZoneVariableValuePredicate<VT>> createZoneVariableValue(
			String name, Zone zone, ZoneVariable<VT> zoneVariable, VT value);
}
