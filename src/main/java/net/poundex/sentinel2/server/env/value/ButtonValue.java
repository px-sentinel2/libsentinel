package net.poundex.sentinel2.server.env.value;

public record ButtonValue(int pushCount, Type type) implements Value<ButtonValue> {
	public enum Type {
		PUSHED, RELEASED_AFTER_HOLD, BEING_HELD
	}
}
