package net.poundex.sentinel2.server.env.zonevariable;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.server.env.value.NumericValue;
import net.poundex.sentinel2.server.env.value.ValueWithMetadata;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@SuperBuilder
public class NumericZoneVariable extends ZoneVariable<NumericValue> {
	
	private final NumericFoldStrategy zoneFoldStrategy;

	@Override
	public NumericValue fold(Collection<ValueWithMetadata<NumericValue>> values) {
		return zoneFoldStrategy.zoneFn.apply(values);
	}

	@RequiredArgsConstructor
	public enum NumericFoldStrategy implements FoldStrategy<NumericValue> {
		
		AVERAGE(vs -> new NumericValue(vs.stream()
				.map(ValueWithMetadata::value)
				.collect(Collectors.averagingDouble(NumericValue::value))));
		
		private final Function<Collection<ValueWithMetadata<NumericValue>>, NumericValue> zoneFn;

		@Override
		public NumericValue fold(Collection<ValueWithMetadata<NumericValue>> values) {
			return zoneFn.apply(values);
		}
	}
}
