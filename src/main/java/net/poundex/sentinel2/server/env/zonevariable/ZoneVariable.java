package net.poundex.sentinel2.server.env.zonevariable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.env.value.Value;
import net.poundex.sentinel2.server.env.value.ValueWithMetadata;

import java.util.Collection;
import java.util.Optional;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
public abstract class ZoneVariable<VT extends Value<VT>> implements SentinelObject {
	
	private final String id;
	private final String name;
	private final Optional<VT> defaultValue;
	
	public abstract VT fold(Collection<ValueWithMetadata<VT>> values);
}
