package net.poundex.sentinel2.server.env.action;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.SentinelEnvironment;

import java.util.List;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
@Slf4j
public class RunFirstConditionalActionAction implements Action, SentinelObject {
	private final String id;
	private final String name;
	private final List<ConditionalAction> actions;
	
	@Override
	public void run(SentinelEnvironment environment) {
		actions.stream()
				.filter(x -> x.getPredicate().test(environment))
				.findFirst()
				.ifPresent(a -> a.run(environment));
	}
}
