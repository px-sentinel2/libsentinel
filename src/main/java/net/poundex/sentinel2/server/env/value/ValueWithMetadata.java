package net.poundex.sentinel2.server.env.value;

import java.time.Instant;

public record ValueWithMetadata<VT extends Value<VT>>(VT value, Instant timestamp) { }
