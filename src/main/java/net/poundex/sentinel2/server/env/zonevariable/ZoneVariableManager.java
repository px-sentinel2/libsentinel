package net.poundex.sentinel2.server.env.zonevariable;

import net.poundex.sentinel2.server.Zone;
import net.poundex.sentinel2.server.env.value.Value;

import java.util.Optional;

public interface ZoneVariableManager {
	<VT extends Value<VT>> Optional<VT> getValue(ZoneVariable<VT> zoneVariable, Zone zone);
	<VT extends Value<VT>> void setValue(ZoneVariable<VT> zoneVariable, Zone zone, VT value);
}
