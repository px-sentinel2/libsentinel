package net.poundex.sentinel2.server.appliance;

public enum ApplianceRole {
	SCENE,
	
	LIGHT,
	
	COLOUR;
}
