package net.poundex.sentinel2.server.appliance;

import net.poundex.sentinel2.ReadOnlyRepository;
import net.poundex.sentinel2.server.Zone;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Set;

public interface ApplianceRepository extends ReadOnlyRepository<Appliance> {
	Mono<Appliance> create(String name, Zone zone, URI deviceId, ApplianceRole... roles);
	Flux<Appliance> findAllByZoneWithRoles(Zone zone, Set<ApplianceRole> roles);
}
