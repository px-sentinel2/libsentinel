package net.poundex.sentinel2.server.appliance;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.Zone;

import java.net.URI;
import java.util.Set;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
public class Appliance implements SentinelObject {
	
	private final String id;
	private final String name;
	private final Zone zone;
	private final URI deviceId;
	private final Set<ApplianceRole> roles;
}
