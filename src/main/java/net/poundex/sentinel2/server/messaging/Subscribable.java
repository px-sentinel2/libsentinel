package net.poundex.sentinel2.server.messaging;

import java.util.function.Consumer;

public interface Subscribable<T> {
	void subscribe(Consumer<T> consumer);
}
