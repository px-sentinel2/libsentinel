package net.poundex.sentinel2.server.button;

import net.poundex.sentinel2.ReadOnlyRepository;
import net.poundex.sentinel2.server.env.action.Action;
import reactor.core.publisher.Mono;

public interface ButtonRepository extends ReadOnlyRepository<Button> {
	Mono<Button> create(String name, Action action);
}
