package net.poundex.sentinel2.server.button;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.env.action.Action;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
@ToString(onlyExplicitlyIncluded = true)
public class Button implements SentinelObject {
	
	@ToString.Include
	private final String id;
	@ToString.Include
	private final String name;
	private final Action action;
}
