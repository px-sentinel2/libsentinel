package net.poundex.sentinel2.server.button;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import net.poundex.sentinel2.SentinelObject;
import net.poundex.sentinel2.server.env.value.ButtonValue;

import java.net.URI;

@Data
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
public class ButtonSource implements SentinelObject {
	
	private final String id;
	private final Button button;
	private final URI source;
	private final ButtonValue value;
}