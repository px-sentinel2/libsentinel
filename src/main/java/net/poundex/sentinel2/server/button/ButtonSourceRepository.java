package net.poundex.sentinel2.server.button;

import net.poundex.sentinel2.ReadOnlyRepository;
import net.poundex.sentinel2.server.env.value.ButtonValue;
import reactor.core.publisher.Mono;

import java.net.URI;

public interface ButtonSourceRepository extends ReadOnlyRepository<ButtonSource> {
	Mono<ButtonSource> create(Button button, URI source, ButtonValue value);
	Mono<ButtonSource> findBySourceAndValue(URI source, ButtonValue buttonValue);
}
