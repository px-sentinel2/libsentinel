package net.poundex.sentinel2.server;

import net.poundex.sentinel2.server.appliance.ApplianceRepository;
import net.poundex.sentinel2.server.device.DeviceManager;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableManager;
import net.poundex.sentinel2.server.env.zonevariable.ZoneVariableRepository;

public interface SentinelEnvironment {
	DeviceManager getDeviceManager();
	ApplianceRepository getApplianceRepository();
	ZoneVariableManager getZoneVariableManager();
	ZoneVariableRepository getZoneVariableRepository();
}
