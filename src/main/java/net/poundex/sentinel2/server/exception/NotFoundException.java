package net.poundex.sentinel2.server.exception;

import lombok.Getter;

public class NotFoundException extends RuntimeException {
	
	@Getter
	private final String type;
	@Getter
	private final String id;
	
	public NotFoundException(Class<?> klass, String id) {
		super(String.format("Entity of type %s with ID %s not found",
				klass.getSimpleName(), id));
		
		this.type = klass.getSimpleName();
		this.id = id;
	}
}
