package net.poundex.sentinel2;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ReadOnlyRepository<T extends SentinelObject> {
	Flux<T> findAll();

	Mono<T> findById(String id);
	
	Flux<T> findAllById(Iterable<String> ids);
}
